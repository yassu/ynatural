from __future__ import annotations

from itertools import product

import pytest

from ynatural import Natural

ZERO = Natural(frozenset())
ONE = Natural(frozenset([frozenset()]))
TWO = Natural(frozenset([
    frozenset(),
    frozenset([frozenset()]),
]))
THREE = Natural(
    frozenset(
        [
            frozenset(),
            frozenset([frozenset()]),
            frozenset([
                frozenset(),
                frozenset([frozenset()]),
            ])
        ]))


def test_Natural_zero():
    assert Natural.zero() == Natural(frozenset())


NATURAL_NEXT_PARAMS = [
    (ZERO, ONE),
    (ONE, TWO),
    (TWO, THREE),
]


@pytest.mark.parametrize('arg, expect', NATURAL_NEXT_PARAMS)
def test_Natural_next(arg, expect):
    assert arg.next_ == expect


NATURAL_PREV_PARAMS = [
    (ONE, ZERO),
    (TWO, ONE),
    (THREE, TWO),
]


@pytest.mark.parametrize('arg, expect', NATURAL_PREV_PARAMS)
def test_Natural_prev(arg, expect):
    assert arg.prev == expect


NATURAL_FROM_INT_PARAMS = [
    (0, ZERO),
    (1, ONE),
    (2, TWO),
    (3, THREE),
]


@pytest.mark.parametrize('arg, expect', NATURAL_FROM_INT_PARAMS)
def test_Natural_from_int(arg, expect):
    assert Natural.from_int(arg) == expect

    with pytest.raises(ValueError):
        Natural.from_int(-1)


def test_Natural_add():
    until_n = 6
    for n1_i, n2_i in product(range(until_n), repeat=2):
        n1 = Natural.from_int(n1_i)
        n2 = Natural.from_int(n2_i)
        assert int(n1 + n2) == n1_i + n2_i


def test_Natural_mul():
    until_n = 6
    for n1_i, n2_i in product(range(until_n), repeat=2):
        n1 = Natural.from_int(n1_i)
        n2 = Natural.from_int(n2_i)
        assert int(n1 * n2) == n1_i * n2_i


NATURAL_INT_PARAMS = [
    (ZERO, 0),
    (ONE, 1),
    (TWO, 2),
    (THREE, 3),
]


@pytest.mark.parametrize('arg, expect', NATURAL_INT_PARAMS)
def test_Natural_int(arg, expect):
    assert int(arg) == expect
