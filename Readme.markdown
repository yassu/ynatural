ynatural
================================================================================

[![pipeline status](https://gitlab.com/yassu/ynatural/badges/master/pipeline.svg)](https://gitlab.com/yassu/ynatural/-/commits/master)


自然数の型の実装.


[Swiftで自然数を作ってみた（ペアノの公理）](https://qiita.com/taketo1024/items/2ab856d21bf9b9f30357)のPython実装.


LICENSE
-------

[MIT](https://gitlab.com/yassu/ynatural/blob/master/LICENSE)
