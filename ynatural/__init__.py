from __future__ import annotations

from dataclasses import dataclass


@dataclass
class Natural:
    value_set: frozenset

    @classmethod
    def zero(cls) -> Natural:
        return Natural(frozenset())

    @property
    def next_(self) -> Natural:
        return Natural(self.value_set | frozenset([self.value_set]))

    @property
    def prev(self) -> Natural:
        return Natural(max(self.value_set))

    @classmethod
    def from_int(cls, n: int) -> Natural:
        if n < 0:
            raise ValueError(f'{n} is a negative number.')

        if n == 0:
            return Natural(frozenset())
        else:
            old_natural_set = cls.from_int(n - 1).value_set
            return Natural(old_natural_set | frozenset([old_natural_set]))

    def __add__(self, other: Natural) -> Natural:
        if other.value_set == frozenset():
            return self
        else:
            return (self + (other.prev)).next_

    def __mul__(self, other) -> Natural:
        if other.value_set == frozenset():
            return Natural(frozenset())
        else:
            return (self * (other.prev)) + self

    def __int__(self) -> int:
        return len(self.value_set)
